let mysql = require('mysql')

var state = {
    pool: null,
    mode: null,
}

let dbConnectionJK = {
    database : null,
    user : null,
    password : null,
    host : null,
    mode : null,
    connectionLimit : 1,
    multipleStatements : false
}

exports.setConnection = ({ database, user, password , host , mode, connectionLimit, multipleStatements })  => {
    dbConnectionJK.database = database
    dbConnectionJK.user = user
    dbConnectionJK.password = password
    dbConnectionJK.host = host
    dbConnectionJK.mode = mode 
    dbConnectionJK.connectionLimit = connectionLimit || 1
    dbConnectionJK.multipleStatements = multipleStatements || false
}
exports.getConnectionDetails = () => { return dbConnectionJK }
exports.get = () => { return state.pool }
exports.getCurrentMode = () => { return state.mode }

exports.connect = function(done) {

    state.pool = mysql.createPool({
      connectionLimit : dbConnectionJK.connectionLimit,
      host: dbConnectionJK.host,
      user: dbConnectionJK.user,
      password: dbConnectionJK.password,
      database: dbConnectionJK.database,
      multipleStatements: true
    })

    state.mode = dbConnectionJK.mode
    done()
}  


let mysql_connection = require('./library')
const resources = require('./resources')

exports.connect = (conn) => {
    mysql_connection.setConnection(conn)
    mysql_connection.connect(function(err) {
        if (err) throw err
        console.log(resources.connection_success)
    })
}

exports.get = () => mysql_connection.get()
exports.mode = () => mysql_connection.getCurrentMode()


